# Promises, async/await, and exception handling exploration

Answer each question in the **Questions** section below
by constructing one or more working code examples that demonstrates the point.

Note, "working" means that the code behaves as expected.
For example, if Node raises a syntax error when the example runs,
but that is the intended result because you are demonstrating that JavaScript
does not have a `foreach` statement, then that may in fact be working code.
However, you must have a comment that explains the error and how that
supports your answer.

Each example must run from the root of the project with a command similar to:

```bash
docker-compose run --rm node examples/q1.js
```

`examples/lib.js` defines some functions that may be useful in your examples.
You don't have to use them if you don't want to.

## Worked Examples

1. Explain exception handling.

    Run `examples/we1.js` and read its comments.

2. What are the different types of exceptions?

    Run `examples/we3.js` and read its comments.

3. What happens when a promise object that returns is rejected?

    Run `examples/we2.js` and read its comments.

## Questions

Place your answers in files named `examples/q1.js`, `examples/q2.js`, etc.
If you need multiple separate examples to answer a question, add a letter
to the name: e.g., `examples/q1a.js`, `examples/q1b.js`, etc.

1. Does the await keyword mean that the program will wait until the promise is
   settled before continuing to the next line?

2. Are you able to use multiple catches for async functions?

3. So async can be used without await, but await can only be used with async
   right?

4. If I executed an asynchronous method but didn’t await it would that be a
   problem?

## Setup

Fork this project into your private, individual group under this class's
group on GitLab: `https://gitlab.com/wne-csit-jackson/it350/individual/YOUR_LAST_NAME` .

Clone your fork to your local system, change into the root of your project,
and start your development environment.

```bash
git clone PASTE_CLONE_URL_FOR_YOUR_FORK
cd Homework3
code .
```

## Working

As you work stage, commit, and push your work.
Assuming you are in the root of your project...

```bash
git add .
git commit -m "short descriptive message about your changes"
git push
```

You should occassionally confirm that your fork on GitLab has your
most recent commits.

## Submission

When you are done, stage, commit, and push your last changes,
confirm that your fork has received the commits, and you are done.
